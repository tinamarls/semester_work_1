package service;

import dto.FileDto;

public interface FileService {

    String save(FileDto fileDto);
}
